package main;

public class CheesePizza {// сырная пицца
    private String composition = "Cheese";
    private Memento memento;

    public void makeCheesePizza(Pizza pizza) {
        this.memento = pizza.getMemento();
        pizza.setComposition(pizza.getComposition() + "," +this.composition);
        pizza.setPrice(pizza.getPrice() + 10);
    }

    public Memento getMemento() {
        return memento;
    }
}
