package main;

public class Pizza {
    private int price;
    private String composition;

    public Pizza(int price, String composition) {
        this.price = price;
        this.composition = composition;
    }

    public int getPrice() {
        return this.price;
    }

    public String getComposition() {
        return this.composition;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setComposition(String composition) {
        this.composition = composition;
    }

    public Memento getMemento() {
        return new Memento(price, composition);
    }

    public void restore(Memento memento) {
        setComposition(memento.getComposition());
        setPrice(memento.getPrice());
    }

    @Override
    public String toString() {
        return "Pizza{" +
                "price=" + price +
                ", composition='" + composition + '\'' +
                '}';
    }
}
