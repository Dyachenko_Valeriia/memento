package test;

import main.*;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestMemento {
    @Test
    public void testCheesePizza() {
        Pizza pizza = new Pizza(50, "Standard");
        CheesePizza cheesePizza = new CheesePizza();
        cheesePizza.makeCheesePizza(pizza); // На этом этапе price = 60, composition = "Standard,Cheese"
        assertEquals(60, pizza.getPrice());
        assertEquals("Standard,Cheese", pizza.getComposition());
        pizza.restore(cheesePizza.getMemento()); //Вернули в первоначальное состояние
        assertEquals(50, pizza.getPrice());
        assertEquals("Standard", pizza.getComposition());
    }
}
